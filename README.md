# chebiExplorer

Utility functions for identifying [ChEBI](https://www.ebi.ac.uk/chebi/) classes of interest, or retrieving portions of ChEBI.


# Todo:


# Related bibliography

- Galgonek, J., Hurt, T., Michlíková, V., Onderka, P., Schwarz, J., & Vondrášek, J. (2016). Advanced SPARQL querying in small molecule databases. Journal of cheminformatics, 8, 31. https://doi.org/10.1186/s13321-016-0144-4 [PMID:27275187](https://pubmed.ncbi.nlm.nih.gov/27275187/)
- Galgonek, J., & Vondrášek, J. (2021). IDSM ChemWebRDF: SPARQLing small-molecule datasets. Journal of cheminformatics, 13(1), 38. https://doi.org/10.1186/s13321-021-00515-1 [PMID:33980298](https://pubmed.ncbi.nlm.nih.gov/33980298/)

